# ns_Startup Chat Server v0.0.02 for ns_Startup

[![Vimeo](https://i.vimeocdn.com/video/823525142.jpg)](https://player.vimeo.com/video/367236737 "Vimeo")(Click to start Vimeo teaser)

Simple Chatserver for ns_Startup:

![Inline image](docs/ns_Startup_Server_Screener.jpg)

https://gitlab.com/e_noni/ns_startup

(Windows only right now, i still working on a linux version with json packages)

- you need a proper Python 2.7 installation
- activate the virtualenv ns_Startup\venv\Scripts\activate.bat
- run the ns_Startup\run_ns_Startup_Server.bat
- check the init paths in the ns_Startup.py